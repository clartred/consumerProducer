/**
 * @Author Nemanja Rajkovic
 * @Date 8.2.2018.
 * This class is representing Goods which
 * the producer will create and consumer will take.
 */
public class Goods {

    private static int goods;
    private static int maximumStorage, minimumStorage;
    private static final Object key = new Object();

    public Goods() {
        this.maximumStorage = 10;
        this.minimumStorage = 0;
    }

    /**
     * This method will allow Consumer to consume the goods.
     * (goods++)
     */
    public void consumeGoods() {
        this.goods = goods - 1;
    }

    /**
     * This method allows Producer to create more goods.
     * (goods--)
     */
    public void produceGoods() {
        this.goods = goods + 1;
    }

    public static int getMaximumStorage() {
        return maximumStorage;
    }

    public static int getMinimumStorage() {
        return minimumStorage;
    }

    public static Object getKey() {
        return key;
    }

    public static int getGoods() {
        return goods;
    }
}