/**
 * @Author Nemanja Rajkovic
 * @Date 8.2.2018.
 * This class is representing Consumer in "Consumer-Producer Problem".
 */
public class Consumer implements Runnable {

    Goods goods = new Goods();

    /**
     * This method will remove goods from storage as long as
     * there is any goods in the storage(goods > 0).
     * When the storage is empty, it will call key.wait()
     * so the producer can produce more goods.
     */
    public synchronized void takeGoods() throws InterruptedException {

        synchronized (goods.getKey()) {
            if (goods.getGoods() == goods.getMinimumStorage()) {
                goods.getKey().wait();
            }
            goods.consumeGoods();
            System.out.println("Consumer's count is: " + goods.getGoods());
            goods.getKey().notifyAll();
        }
    }

    /**
     * run() method is implemented from Interface Runnable.
     */
    @Override
    public void run() {
        System.out.println(goods.getKey());
        while (true) {
            try {
                takeGoods();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}