/**
 * @Author Nemanja Rajkovic
 * @Date 8.2.2018.
 * This class is representing Producer in "Consumer-Producer Problem".
 */
public class Producer implements Runnable {

    Goods goods = new Goods();

    /**
     * This method will add goods as long as it can but when
     * the storage is full, it will cause thread to wait(by calling key.wait())
     * so the Consumer can take goods from storage.
     */
    public synchronized void addGoods() throws InterruptedException {
        synchronized (goods.getKey()) {
            if (goods.getGoods() == goods.getMaximumStorage()) {
                goods.getKey().wait();
            }
            goods.produceGoods();
            System.out.println("Producer's count is: " + goods.getGoods());
            goods.getKey().notifyAll();
        }
    }

    /**
     * run() method is implemented from Interface Runnable.
     */
    @Override
    public void run() {
        System.out.println(goods.getKey());
        while (true) {
            try {
                addGoods();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}